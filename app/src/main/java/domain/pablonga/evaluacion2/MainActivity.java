package domain.pablonga.evaluacion2;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import domain.pablonga.evaluacion2.Fragmentos.DatosPersonalesFragment;
import domain.pablonga.evaluacion2.Fragmentos.DatosdeContactoFragment;
import domain.pablonga.evaluacion2.Fragmentos.InicioSesion;

public class MainActivity extends AppCompatActivity implements
        InicioSesion.OnFragmentInteractionListener,
        DatosdeContactoFragment.OnFragmentInteractionListener,
        DatosPersonalesFragment.OnFragmentInteractionListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        DatosPersonalesFragment datosPersonalesFragment = new DatosPersonalesFragment();

        transaction.replace(R.id.f1Contenedor, datosPersonalesFragment).commit();


    }

    @Override
    public void onFragmentInteraction(String nombreFragmento, String evento) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (nombreFragmento.equals("DatosPersonalesFragment")){
            if (evento.equals("BTN_SIGUIENTE")){
                DatosdeContactoFragment fragment = new DatosdeContactoFragment();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        if (nombreFragmento.equals("DatosContactoFragment")){
            if (evento.equals("BTN_SIGUIENTE")){
                InicioSesion fragment = new InicioSesion();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        if (nombreFragmento.equals("DatosSesionFragment")){
            if (evento.equals("BTN_SIGUIENTE")){

            }
        }
    }
}
